# lifeOfJorge

## Objetivo
    O objetivo do trabalho é desenvolver um jogo interativo estilo FruitNinja que se baseia nas dificuldades da vida de um homem, Jorge, o nome do jogo é Life Of Jorge. 
    
## Ferramenta
    Usamos a engine Godot para fazer o jogo e na mesma exportamos para plataforma Android
    
## Linguagem
    -GDScript
     
## Links
*    [Godot] (https://godotengine.org/)

## Autores do Projeto
    -Gabriel Nunes
    -Leandro Augusto
    -Ygor Costa

## Agradecimento
    Agradecemos aos professores pelo incentivo e pela oportunidade de fazer o jogo.